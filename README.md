# s0s website

A student website built with [Zola](https://www.getzola.org/) powered by 🌱 made with 💚 for 🌍 in 🇮🇸

No scripts, no tracking, no cruelty

Free as in freedom, feel free to contribute

[s0s.is](https://s0s.is/)

## Setup

See the official [setup guide](https://www.getzola.org/documentation/getting-started/installation/) by Zola

Get a live preview with:
```
zola serve
```

## Building

Just run:
```
zola build
```

## Acknowledgement

Images:
- Logo "Sprout disc icon" by [Lorc](https://lorcblog.blogspot.com/) on [Game-icons.net](https://game-icons.net/1x1/lorc/sprout-disc.html)
- "Down Arrow 5" by [Ankush Syal](https://www.svgrepo.com/author/Ankush%20Syal/) on [SVG Repo](https://www.svgrepo.com/svg/520696/down-arrow-5)
- Background "Mývatn" by [Richard Dorran](https://unsplash.com/@dozza88) on [Unsplash](https://unsplash.com/photos/pink-petaled-flowers-near-body-of-water-during-daytime-photo-KkTP4f7jAok)

Zola theme inspiration:
- [Zola ʕ•ᴥ•ʔ Bear Blog](https://codeberg.org/alanpearce/zola-bearblog)
- [Juice](https://github.com/huhu/juice/)

## License

This work is [licensed](LICENSE) under [Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
