+++
title = "Um okkur"
template = "page.html"

[extra]
goals_text = "Okkar markmið"
goals = "<p>Við viljum efla þekkingu allra en einkum annarra háskólanema á umhverfisáhrifum dýraafurða og ómannúðlegri meðferð dýra til þess að hvetja fólk til að draga úr eða hætta neyslu dýraafurða.</p><p>Við viljum skora á Félagsstofnun Stúdenta sem rekur matsölu á háskólasvæði Háskóla Íslands að auka enn frekar úrval plöntumiðaðs (vegan) fæðis í Hámu, á Stúdentakjallaranum og í Bóksölu stúdenta. Jafnframt biðjum við Félagsstofnun Stúdenta að leggja sitt af mörkum til að hvetja viðskiptavini sína til að velja vegan vörur. Nokkrar leiðir til þess eru að selja þær á lægra verði en sambærilegar vörur úr dýraafurðum, að taka fram kolefnisfótspor hverrar vöru á pakkningunum þeirra og að bjóða upp á hollari og saðsamari vegan rétti.</p>"
people_text = "Fólk"
people = [
  {name="Kári", photo="kari.png", story="Kári Thayer er landvörður og nemandi við Háskóla Íslands. Hann er að taka BA gráðu í þjóðfræði með landfræði sem aukagrein. Áhugasvið hans er flókið og síbreytilegt samband mannfólks við náttúru og dýr, ásamt jaðarsetningu minnihlutahópa. Sem landvörður hefur hann unnið í Skaftafelli og á Þingvöllum og næsti vinnustaður verður Dyrhólaey.<br/><br/>Kári hefur verið vegan í 3+ ár og var þar á undan grænmetisæta í nokkur ár. Kveikjan að breyttu mataræði var áfangi í landfræði um loftslagsbreytingar og síðan þá hefur hann haldið áfram að afla sér upplýsinga um dýravelferð og leiðir til þess að minnka kolefnisfótsporið sitt og vill hann miðla þeim til sem flestra. Það tók hann dágóðan tíma og nokkrar tilraunir að verða vegan og hann vill hvetja fólk til þess að sýna sér þolinmæði og skilning og að gefast ekki upp."},
  {name="Gijs", photo="gijs.png", story="Gijs er framhaldsnemi við Háskóla Íslands sem er að ljúka framhaldsnámi í fjarkönnun og stefnir að því að hjálpa náttúrunni með siðferðislegri tækni. Ástríðufullur um réttindi dýra (bæði mennskra og ómennskra), að draga úr loftslagskreppu, efla vistvænan landbúnað og tæknifrelsi - sem er nátengt fyrrnefndum atriðum. Einnig að miðla boðskapnum til almennings og á vefmiðlum með opnum hug í hversdagslegum aðstæðum.<br/><br/>Síðan seint á árinu 2019 hefur Gijs tileinkað sér vegan lífsstíl eftir að hafa hægt og rólega dregið úr neyslu dýraafurða af umhverfisástæðum. Það gerðist í kjölfarið á því að hafa áttað sig á að það er engin góð ástæða til að borga fyrir iðnað fullan af dýraníði. Í leiðinni uppgötvaði Gijs nýjan heim af mat og lífsstíl."},
]
+++

