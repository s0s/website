+++
title = "About"
template = "page.html"

[extra]
goals_text = "Our goals"
goals = "<p>We want to increase people's knowledge but especially our fellow university students' knowledge of the environmental impact of animal products and the abusive treatment of animals to encourage people to stop the consumption of animal products.</p><p>We challenge Félagsstofnun Stúdenta (FS) who runs the sale of food on the campus of the University of Iceland to further increase the options of plant-based (vegan) options at Háma, Stúdentakjallarinn and Bóksala stúdenta. Furthermore, we ask Félagsstofnun Stúdenta to do what's in their power to encourage their customers to choose vegan options. A few ways to do that would be to sell vegan options at a lower price than their counterparts containing animal products, to label each product with its CO<sub>2</sub> footprint, and to offer healthier and more filling vegan dishes.</p>"
people_text = "People"
people = [
  {name="Kári", photo="kari.png", story="Kári Thayer is a park ranger and a student at the University of Iceland. He's doing a bachelor's in ethnology with a minor in geography. His field of interest is the complex and ever-changing relationship between humans, nature, and animals, along with an interest in the marginalization of minority groups. As a park ranger, he has worked in Vatnajökull national park, Þingvellir national park and his next stop is Dyrhólaey.<br/><br/>Kári has been vegan for 3+ years and before that he was a vegetarian for a few years. The inspiration for a change in diet came from a geography course about climate change and since then, he has continued to seek information about animal welfare and ways to reduce his ecological footprint. He wants to spread this knowledge to as many people as possible. It took him some time and a few tries to become vegan and he wants to encourage people to show themselves patience and understanding and to not give up trying."},
  {name="Gijs", photo="gijs.png", story="Gijs is a graduate student at the University of Iceland completing a degree in remote sensing, aspiring to help nature using ethical technology. Passionate about animal rights issues (both human and non-human), regenerative agriculture, and technological freedom - which is surprisingly interconnected to the former topics. Spreading awareness to peers and on online platforms through casual and open-minded conversation.<br/><br/>Since late 2019, Gijs has adopted a vegan lifestyle after slowly reducing animal products for environmental reasons. Later realizing there was no excuse to keep paying for animal cruelty, and in the process discovering a whole new world of food and living."}
]
+++

