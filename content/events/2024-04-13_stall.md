+++
title = "SHÍ and FS market"

[extra]
start = "13:00"
end = "16:00"
location = "University Centre, University of Iceland"
coords = "https://www.openstreetmap.org/?mlat=64.13985&mlon=-21.95033#map=16/64.13985/-21.95033"
+++

SOS will run a stall at an event held by SHÍ and FS where students will be selling clothes, art and more. We will invite people to taste vegan cheeses from Livefood for free and we'll provide information about the harmful practices in animal agriculture. Come check us out! 
